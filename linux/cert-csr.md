# How to generate Certificate request (CSR)

```bash
DOMAIN=example.com openssl req -sha256 -nodes -newkey rsa:2048 -keyout $DOMAIN.key -out $DOMAIN.csr
```
