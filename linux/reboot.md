## Disclaimer 

**Warning** : these reboot methods may harm your system/devices. Please be careful and take time to google any solution before trying it. We do not take any responsibility about what you will do with theses commands.

## Option 1 : standard reboot

Seriously, always choose this way when possible

```bash
reboot
```

## Option 2 : force reboot

Warning : very rude method !

```bash
systemctl reboot --force --force
```

## Option 3 : sysrq-trigger

Warning : very very rude method !

```bash
echo -e "r\ne\ni\ns\nu\nb\n" > /proc/sysrq-trigger
```

