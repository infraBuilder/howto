# How to setup HELM in cluster with RBAC

Helm is made of a client and a server running in your cluster, named Tiller. On RBAC enabled cluster (all cluster should), you have to setup a ServiceAccount for Tiller and mandatory rights.

## Part 1 : Create ServiceAccount 

You can do all (ServiceAccount + ClusterRoleBinding) in one command :

```
kubectl apply -f - <<EOF
apiVersion: v1
kind: ServiceAccount
metadata:
  name: tiller
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: tiller
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - kind: ServiceAccount
    name: tiller
    namespace: kube-system
EOF
```

## Part 2 : Setup Tiller with its ServiceAccount

Just with :

```
helm init --service-account tiller
```

