# Docker registry on s3 bucket

https://docs.docker.com/registry/storage-drivers/s3/

```
apiVersion: v1
kind: Namespace
metadata:
  name: docker-registry
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: config
  namespace: docker-registry
data:
  config.yml: |-
    version: 0.1
    http:
      addr: 0.0.0.0:5000
    log:
      accesslog:
        disabled: true
      level: info
      formatter: text
      fields:
        service: registry
    storage:
      s3:
        accesskey: D7CINFRABUILDERKA6DY
        secretkey: LgzVZZDA483inFrABuILDercNtFgP2LIlgXmWfGr
        region: us-west-1
        regionendpoint: https://s3.endpoint.cephgw.local
        bucket: registry
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: docker-registry
  name: registry
  namespace: docker-registry
spec:
  selector:
    matchLabels:
      app: docker-registry
  template:
    metadata:
      labels:
        app: docker-registry
    spec:
      containers:
      - image: registry:2
        name: registry
        ports:
        - containerPort: 5000
          name: http
          protocol: TCP
        volumeMounts:
        - mountPath: /etc/docker/registry
          name: config
      volumes:
      - configMap:
          name: config
        name: config
```
