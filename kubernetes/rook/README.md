# Rook 

## Ceph

### Install

#### 1 : deploy operator

```bash
kubectl apply -f https://github.com/rook/rook/blob/release-1.1/cluster/examples/kubernetes/ceph/common.yaml
```

#### 2 : deploy operator

```bash
kubectl apply -f https://github.com/rook/rook/blob/release-1.1/cluster/examples/kubernetes/ceph/operator.yaml
```

#### 3 : deploy Ceph cluster

```yaml
apiVersion: ceph.rook.io/v1
kind: CephCluster
metadata:
  name: rook-ceph
  namespace: rook-ceph
spec:
  cephVersion:
    image: ceph/ceph:v14.2.4-20190917
  dashboard:
    enabled: true
    port: 8000
  dataDirHostPath: /var/lib/rook
  disruptionManagement: {}
  external:
    enable: false
  mgr:
    modules:
    - enabled: true
      name: pg_autoscaler
  mon:
    count: 3
  monitoring: {}
  network:
    hostNetwork: false
    provider: ""
    selectors: null
  placement:
    mgr:
      nodeAffinity:
        requiredDuringSchedulingIgnoredDuringExecution:
          nodeSelectorTerms:
          - matchExpressions:
            - key: node-role.kubernetes.io/controlplane
              operator: In
              values:
              - "true"
      tolerations:
      - key: node-role.kubernetes.io/etcd
        operator: Exists
      - key: node-role.kubernetes.io/controlplane
        operator: Exists
    mon:
      nodeAffinity:
        requiredDuringSchedulingIgnoredDuringExecution:
          nodeSelectorTerms:
          - matchExpressions:
            - key: node-role.kubernetes.io/controlplane
              operator: In
              values:
              - "true"
      tolerations:
      - key: node-role.kubernetes.io/etcd
        operator: Exists
      - key: node-role.kubernetes.io/controlplane
        operator: Exists
  rbdMirroring:
    workers: 0
  storage:
    useAllDevices: true
    useAllNodes: true
```

#### 4 : Create RBD block storage class

```yaml
apiVersion: ceph.rook.io/v1
kind: CephBlockPool
metadata:
  name: replicapool
  namespace: rook-ceph
spec:
  failureDomain: host
  replicated:
    size: 3
---
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
   name: rook-ceph-block
# Change "rook-ceph" provisioner prefix to match the operator namespace if needed
provisioner: rook-ceph.rbd.csi.ceph.com
parameters:
    # clusterID is the namespace where the rook cluster is running
    clusterID: rook-ceph
    # Ceph pool into which the RBD image shall be created
    pool: replicapool

    # RBD image format. Defaults to "2".
    imageFormat: "2"

    # RBD image features. Available for imageFormat: "2". CSI RBD currently supports only `layering` feature.
    imageFeatures: layering

    # The secrets contain Ceph admin credentials.
    csi.storage.k8s.io/provisioner-secret-name: rook-ceph-csi
    csi.storage.k8s.io/provisioner-secret-namespace: rook-ceph
    csi.storage.k8s.io/node-stage-secret-name: rook-ceph-csi
    csi.storage.k8s.io/node-stage-secret-namespace: rook-ceph

    # Specify the filesystem type of the volume. If not specified, csi-provisioner
    # will set default as `ext4`.
    csi.storage.k8s.io/fstype: xfs

# Delete the rbd volume when a PVC is deleted
reclaimPolicy: Delete
```

#### 5 : Ceph Filesystem storage class

```yaml
apiVersion: ceph.rook.io/v1
kind: CephFilesystem
metadata:
  name: myfs
  namespace: rook-ceph
spec:
  metadataPool:
    replicated:
      size: 3
  dataPools:
    - replicated:
        size: 3
  metadataServer:
    activeCount: 1
    activeStandby: true
---
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: rook-cephfs
# Change "rook-ceph" provisioner prefix to match the operator namespace if needed
provisioner: rook-ceph.cephfs.csi.ceph.com
parameters:
  # clusterID is the namespace where operator is deployed.
  clusterID: rook-ceph

  # CephFS filesystem name into which the volume shall be created
  fsName: myfs

  # Ceph pool into which the volume shall be created
  # Required for provisionVolume: "true"
  pool: myfs-data0

  # Root path of an existing CephFS volume
  # Required for provisionVolume: "false"
  # rootPath: /absolute/path

  # The secrets contain Ceph admin credentials. These are generated automatically by the operator
  # in the same namespace as the cluster.
  csi.storage.k8s.io/provisioner-secret-name: rook-ceph-csi
  csi.storage.k8s.io/provisioner-secret-namespace: rook-ceph
  csi.storage.k8s.io/node-stage-secret-name: rook-ceph-csi
  csi.storage.k8s.io/node-stage-secret-namespace: rook-ceph

reclaimPolicy: Delete
```

#### 6 : deploy toolbox

```bash
kubectl apply -f https://raw.githubusercontent.com/rook/rook/release-1.1/cluster/examples/kubernetes/ceph/toolbox.yaml
```

### ObjectStorage

#### 1 : Creating object store (s3 endpoint)

```yaml
apiVersion: ceph.rook.io/v1
kind: CephObjectStore
metadata:
  name: mystore
  namespace: rook-ceph
spec:
  # The pool spec used to create the metadata pools. Must use replication.
  metadataPool:
    failureDomain: host
    replicated:
      size: 3
  # The pool spec used to create the data pool. Can use replication or erasure coding.
  dataPool:
    failureDomain: host
    erasureCoded:
      dataChunks: 2
      codingChunks: 1
  # The gaeteway service configuration
  gateway:
    # type of the gateway (s3)
    type: s3
    # A reference to the secret in the rook namespace where the ssl certificate is stored
    sslCertificateRef:
    # The port that RGW pods will listen on (http)
    port: 80
    # The port that RGW pods will listen on (https). An ssl certificate is required.
    securePort:
    # The number of pods in the rgw deployment
    instances: 1
    # The affinity rules to apply to the rgw deployment or daemonset.
    placement:
    #  nodeAffinity:
    #    requiredDuringSchedulingIgnoredDuringExecution:
    #      nodeSelectorTerms:
    #      - matchExpressions:
    #        - key: role
    #          operator: In
    #          values:
    #          - rgw-node
    #  tolerations:
    #  - key: rgw-node
    #    operator: Exists
    #  podAffinity:
    #  podAntiAffinity:
    # A key/value list of annotations
    annotations:
    #  key: value
    resources:
    # The requests and limits set here, allow the object store gateway Pod(s) to use half of one CPU core and 1 gigabyte of memory
    #  limits:
    #    cpu: "500m"
    #    memory: "1024Mi"
    #  requests:
    #    cpu: "500m"
    #    memory: "1024Mi"
```

#### 2 : Creating store user

```yaml
apiVersion: ceph.rook.io/v1
kind: CephObjectStoreUser
metadata:
  name: mystore-admin
  namespace: rook-ceph
spec:
  store: mystore
  displayName: "mystore admin"kubectl get secret rook-ceph-object-user-mystore-mystore-admin -o yaml

```

By creating this user, a new secret will be available in its namespace named `rook-ceph-object-user-mystore-mystore-admin`

```bash
kubectl -n rook-ceph get secret rook-ceph-object-user-tooling-tooling-admin -o yaml| \
  grep ".*Key:" |while read key val; do echo "$key $(base64 -d <<< $val)"; done
```

#### 3 : Manipulating buckets

```bash
export AWS_HOST=rook-ceph-rgw-mystore.rook-ceph # hostname[:port] or ip[:port]
export AWS_ACCESS_KEY_ID=B0JE95L2V6NACF123BJ8
export AWS_SECRET_ACCESS_KEY=kAD8m12iicl1fcAQ2mR1qfgYoSWaWR2WcT9wtzv7

# Creating bucket
s3cmd mb --no-ssl --host=${AWS_HOST} --region=":default-placement" --host-bucket="" s3://rookbucket

# Uploading to bucket 
echo "Hello Rook" > /tmp/rookObj
s3cmd put /tmp/rookObj --no-ssl --host=${AWS_HOST} --host-bucket="" s3://rookbucket/

# Downloading from bucket
s3cmd get s3://rookbucket/rookObj /tmp/rookObj-downloaded --no-ssl --host=${AWS_HOST} --host-bucket="" 

# Listing files in bucket
s3cmd ls s3://rookbucket --no-ssl --host=${AWS_HOST} --host-bucket="" 

```

### Pool configurations

#### RBD RF3

```yaml
apiVersion: ceph.rook.io/v1
kind: CephBlockPool
metadata:
  name: replicapoolrf3
  namespace: rook-ceph
spec:
  failureDomain: host
  replicated:
    size: 3
```


#### RBD RF1

```yaml
apiVersion: ceph.rook.io/v1
kind: CephBlockPool
metadata:
  name: replicapoolrf1
  namespace: rook-ceph
spec:
  failureDomain: host
  replicated:
    size: 1
```

#### RBD Erasure coding

```yaml
apiVersion: ceph.rook.io/v1
kind: CephBlockPool
metadata:
  name: replicapoolec
  namespace: rook-ceph
spec:
  failureDomain: host
  erasureCoded:
    dataChunks: 2
    codingChunks: 1
```

### Manual operations

####  Enable pg_autoscaler on a pool

Enter in rook-ceph-operator :
```bash
kubectl -n rook-ceph exec -it deploy/rook-ceph-operator bash
```

And then enable pg_autoscaler on your pool (exemple with pool `replicapool`) :
```bash
ceph osd pool set replicapool pg_autoscale_mode on
```

Verify autoscaler status :
```bash
ceph osd pool autoscale-status
```

####  Enable pg_autoscaler on a pool

Enter in rook-ceph-operator :
```bash
kubectl -n rook-ceph exec -it deploy/rook-ceph-operator bash
```

And then change dashboard port :
```bash
ceph config set mgr.a mgr/dashboard/server_port 8444
```