# How to create a user in Kubernetes

For more information : https://kubernetes.io/docs/reference/access-authn-authz/authentication/

## Step 1 : create a CSR 

On your computer, generate a CSR :

```bash
USERNAME=alexis.ducastel
openssl req -out $USERNAME.csr -new -newkey rsa:2048 -nodes -keyout $USERNAME.key -subj "/CN=$USERNAME/OU=admin/O=infraBuilder/C=FR/L=Peynier/ST=SUD"
```

## Step 2 : Create a CertificateSigningRequest in Kube

As easy as :

```bash
kubectl apply -f - <<EOF
apiVersion: certificates.k8s.io/v1beta1
kind: CertificateSigningRequest
metadata:
  name: user.$USERNAME
spec:
  request: $(cat $USERNAME.csr | base64 | tr -d '\n')
EOF
```

## Step 3 : Approve CertificateSigning request

Your kubernetes administrator (you ?) has to approve this request :

```bash
kubectl certificate approve user.$USERNAME
```

## Step 4 : download certificate

You just have to retrieve the corresponding certificate :

```bash
kubectl get csr user.$USERNAME -o jsonpath='{.status.certificate}' |base64 -D > $USERNAME.crt
```

Optionnaly, you can check CRT values :

```bash
openssl x509 -text -noout -in $USERNAME.crt
```

## Step 5 : Generate kubeconfig

Now with certificates we can generate a kubeconfig file :

```bash
CONTEXTNAME=$USERNAME
CLUSTER=$(kubectl config view --minify -o jsonpath='{.clusters[0].name}')
DEFAULTNAMESPACE="default"

kubectl config set-credentials $CLUSTER.$USERNAME --client-certificate=$(pwd)/$USERNAME.crt --client-key=$(pwd)/$USERNAME.key --embed-certs=true

kubectl config set-context $CONTEXTNAME --cluster=$CLUSTER --user=$CLUSTER.$USERNAME --namespace $DEFAULTNAMESPACE

kubectl config use-context $CONTEXTNAME
```

